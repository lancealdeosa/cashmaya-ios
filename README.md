# Setup
1. Run `pod install` at the root director
2. Open CoinMaya.xcworkspace

# Notes
- There is a toggle next to the wallet title that simulates the BE returning a custom error. The mock server I've used (https://mockapi.io/) doesn't have a way to trigger HTTP status codes aside from `200`.
- Let me know if there's any issues running the project on your laptops, specifically with the third-party libraries. I've had to do some adjustments to make them run on my M1 Macbook Pro.
- Slow/no internet connections can be simulated using the Network Link Conditioner, available from the Apple Developer site.
- Happy to discuss how I've implemented this demo app. :)