//
//  ListViewModelTests.swift
//  CashMayaTests
//
//  Created by Lance on 10/12/21.
//

import XCTest
@testable import CashMaya

class ListViewModelTests: XCTestCase {
  
  var viewModel: ListViewModel!
  
  override func setUp() {
    super.setUp()
    viewModel = MockListViewModel()
  }
    
  func testContentCount() {
    viewModel.fetchContent { [weak self] _ in
      XCTAssertEqual(self?.viewModel.contentCount, 3)
    }
  }
}

class MockListViewModel: ListViewModel {
  private var strings: [String] = []
  
  var isLoading: Bool = false
  
  var contentCount: Int { return strings.count }
  
  func cellForRowAt(_ indexPath: IndexPath, forTableView tableView: UITableView) -> UITableViewCell {
    return UITableViewCell()
  }
  
  func fetchContent(completion: @escaping (Result<Bool, Error>) -> Void) {
    strings = ["abc", "def", "123"]
    completion(.success(true))
  }
}
