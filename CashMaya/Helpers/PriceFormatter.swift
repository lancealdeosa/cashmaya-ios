//
//  PriceFormatter.swift
//  CashMaya
//
//  Created by Lance on 10/12/21.
//

import Foundation

struct PriceFormatter {
  fileprivate enum Constants {
    static let defaultString = "0.00"
  }
  
  static func format(amount: Double, currency: Currency, showSymbol: Bool = true) -> String {
    let formattedAmount: String

    switch currency {
    case .dollar, .peso:
      formattedAmount = defaultFormatter.string(from: amount)
    case .ethereum:
      formattedAmount = ethereumFormatter.string(from: amount)
    }
    
    if showSymbol {
      return "\(formattedAmount) \(currency.longDisplayString)"
    } else {
      return formattedAmount
    }
  }
  
  private static var defaultFormatter: NumberFormatter = {
    let formatter = NumberFormatter()
    formatter.minimumFractionDigits = 2
    formatter.maximumFractionDigits = 2
    return formatter
  }()
  
  private static var ethereumFormatter: NumberFormatter = {
    let formatter = NumberFormatter()
    formatter.numberStyle = .scientific
    return formatter
  }()
}

fileprivate extension NumberFormatter {
  func string(
    from value: Double,
    defaultValue: String = PriceFormatter.Constants.defaultString
  ) -> String {
    let number = NSNumber(value: value)
    return string(from: number) ?? defaultValue
  }
}
