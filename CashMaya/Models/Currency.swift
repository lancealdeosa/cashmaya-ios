//
//  Currency.swift
//  CashMaya
//
//  Created by Lance on 10/12/21.
//

import Foundation

enum Currency: String, Codable {
  case peso = "PHP"
  case dollar = "USD"
  case ethereum = "ETH"
  
  var shortDisplayString: String {
    switch self {
    case .peso: return "P"
    case .dollar: return "$"
    case .ethereum: return "E"
    }
  }
  
  var longDisplayString: String {
    switch self {
    case .peso: return "PHP"
    case .dollar: return "USD"
    case .ethereum: return "ETH"
    }
  }
}
