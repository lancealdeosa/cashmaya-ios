//
//  Transaction.swift
//  CashMaya
//
//  Created by Lance on 10/11/21.
//

import Foundation

struct Transaction: Codable {
  let id: String
  let entry: Entry
  let amount: Double
  let currency: Currency
  let sender: String
  let recipient: String
  
  enum CodingKeys: String, CodingKey {
    case id
    case entry
    case amount
    case currency
    case sender
    case recipient
  }
}

extension Transaction {
  enum Entry: String, Codable {
    case incoming
    case outgoing
  }
}

struct GetTransactionsResponse: Codable {
  let transactions: [Transaction]
  
  enum CodingKeys: String, CodingKey {
    case transactions = "histories"
  }
}
