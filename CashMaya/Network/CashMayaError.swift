//
//  CashMayaError.swift
//  CashMaya
//
//  Created by Lance on 10/11/21.
//

import Foundation

enum CashMayaError: Error {
  case generic
  case networkFailure
  case custom(CashMayaErrorResponse.CustomError)
}

extension CashMayaError {
  var message: String {
    switch self {
    case .generic:
      return "Oops, something went wrong. Sorry."
    case .networkFailure:
      return "Oops, something's wrong with your internet connection."
    case .custom(let customError):
      return customError.message
    }
  }
}

struct CashMayaErrorResponse: Codable {
  struct CustomError: Codable {
    let code: String
    let message: String
  }
  
  private let errors: [CustomError]
  var error: CustomError { errors.first! }
}
