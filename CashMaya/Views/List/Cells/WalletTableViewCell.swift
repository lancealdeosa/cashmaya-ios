//
//  WalletTableViewCell.swift
//  CashMaya
//
//  Created by Lance on 10/10/21.
//

import UIKit

protocol WalletTableCellViewModel {
  var name: String { get }
  var balanceString: String { get }
}

struct WalletTableCellViewModelImp: WalletTableCellViewModel {
  private let wallet: Wallet
  
  init(wallet: Wallet) {
    self.wallet = wallet
  }
  
  var name: String { wallet.name }
  var balanceString: String { PriceFormatter.format(amount: wallet.balance, currency: wallet.currency, showSymbol: false) }
}

class WalletTableViewCell: UITableViewCell {
  
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var balanceLabel: UILabel!
  
  func update(with model: WalletTableCellViewModel) {
    nameLabel.text = model.name
    balanceLabel.text = model.balanceString
  }
}

extension WalletTableViewCell: NibLoadable {}
