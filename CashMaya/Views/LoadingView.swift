//
//  LoadingView.swift
//  CashMaya
//
//  Created by Lance on 10/11/21.
//

import UIKit

class LoadingView: UIView {

  private lazy var containerView: UIView = {
    let view = UIView().configureForAutoLayout()
    view.cornerRadius = 6
    view.backgroundColor = .white.withAlphaComponent(0.7)
    return view
  }()
  
  private lazy var activityIndicator: UIActivityIndicatorView = {
    let view = UIActivityIndicatorView(style: .large)
    view.configureForAutoLayout()
    view.tintColor = .white
    return view
  }()

  init() {
    super.init(frame: .zero)
    setupView()
  }
  
  required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
  
  func startAnimating() {
    activityIndicator.startAnimating()
  }
  
  func stopAnimating() {
    activityIndicator.stopAnimating()
  }
}

private extension LoadingView {
  func setupView() {
    addSubviews(containerView, activityIndicator)
    containerView.addInnerConstraint(.zero)
    
    NSLayoutConstraint.activate([
      activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
      activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor)
    ])
  }
}
