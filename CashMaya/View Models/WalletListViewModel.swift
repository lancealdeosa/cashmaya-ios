//
//  WalletListViewModel.swift
//  CashMaya
//
//  Created by Lance on 10/11/21.
//

import Foundation
import UIKit

class WalletListViewModelImp: ListViewModel {

  private let apiClient: APIClient
  
  private var wallets: [Wallet] = []
  private(set) var isLoading = false
  
  init(apiClient: APIClient = .shared) {
    self.apiClient = apiClient
  }
  
  var contentCount: Int {
    return wallets.count
  }
  
  func cellForRowAt(_ indexPath: IndexPath, forTableView tableView: UITableView) -> UITableViewCell {
    guard let wallet = wallets[safe: indexPath.row] else {
      return UITableViewCell()
    }
    let cell: WalletTableViewCell = tableView.dequeueReusableCell(for: indexPath)
    let cellModel = WalletTableCellViewModelImp(wallet: wallet)
    cell.update(with: cellModel)
    return cell
  }
  
  func fetchContent(completion: @escaping (Result<Bool, Error>) -> Void) {
    guard !isLoading else { return }
    isLoading = true
    
    apiClient.getWallets(forceError: forceError) { [weak self] result in
      guard let self = self else { return }
      
      self.isLoading = false
      
      switch result {
      case .success(let wallets):
        self.wallets = wallets
        completion(.success(true))
      case .failure(let error):
        completion(.failure(error))
      }
    }
  }
  
  var isForceErrorShown: Bool = true
  private var forceError = false
  
  func toggleForceError(isOn: Bool) {
    forceError = isOn
  }
}
